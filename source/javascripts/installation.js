/* eslint-disable consistent-return */
var getUrlParameter = function getUrlParameter(param) {
  var pageURL = decodeURIComponent(window.location.search.substring(1));
  var urlParams = pageURL.split('&');

  for (var i = 0; i < urlParams.length; i++) {
    var parameterName = urlParams[i].split('=');

    if (parameterName[0] === param) {
      // eslint-disable-next-line no-undefined
      return parameterName[1] === undefined ? true : parameterName[1];
    }
  }
};

$(function() {
  // eslint-disable-next-line no-unused-vars
  var internalNavigationEvent = 'onpopstate' in window ? 'popstate' : 'hashchange';
  var hasVersionParam = (new RegExp('version=ce')).test(window.location.href);
  var scrollToDistro = function scrollToDistro(el) {
    $(window).scrollTop(el.offset().top - 100);
  };
  var showDistro = function showDistroy(el) {
    el.removeClass('hidden')
      .addClass('is-active')
      .prev()
      .addClass('is-active');

    setTimeout(function() {
      scrollToDistro(el);
    });
  };

  if (location.hash) {
    showDistro($(location.hash.split('?')[0]));
  }

  $('.js-edition-picker').toggleClass('hidden', !hasVersionParam);
  $('.js-platform-ce').toggleClass('hidden', !hasVersionParam);
  $('.js-default-ee-notice').toggleClass('hidden', hasVersionParam);
  $('.js-platform-ee').toggleClass('hidden', hasVersionParam);
  $('.js-distro-tile-ce-only').next('.js-distro-content').find('.js-platform-ce').removeClass('hidden');

  var $allDistros = $('.js-distro-content');
  $('.js-distro-tile').on('click', function(e) {
    var isOpen = this.parentNode.classList.contains('is-active');
    $allDistros.addClass('hidden');
    $('.distro-tile.is-active').removeClass('is-active');

    if (!isOpen) {
      showDistro($(this.getAttribute('href')));
    } else {
      e.preventDefault();
      location.hash = '';
      return false;
    }
  });

  if (getUrlParameter('default') === 'ee') {
    $('.js-install-instructions').addClass('hidden');
    $('.js-platform-ee').removeClass('hidden');
  }

  window.initCopyButton('.js-copy-btn');
});

$(function() {
  var $navLIs = $('a.slpEventNavigationLink')
  $navLIs.click(function() {
    $navLIs.removeClass('active');
    $(this).addClass('active');
  });
});

function getPathFromUrl() {
  var url = window.location.href; 
  if(url.indexOf("#") != -1)
     url = url.split("#")[0];
  window.history.replaceState({}, '', url );
}

$(function() {
  var $closeButton = $('a#close-distro')
  var $distroSection = $('li.distro-content')
  $closeButton.click(function() {
    $distroSection.addClass('hidden');
    $distroSection.removeClass('is-active');
    getPathFromUrl()
  });
});

