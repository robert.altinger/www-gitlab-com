---
layout: handbook-page-toc
title: ⛅🌱 Cloud Seed
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## ⛅🌱 Cloud Seed

Cloud Seed is an open-source program lead by GitLab Incubation Engineering in collaboration with Google Cloud.

Currently in `private-testing` mode, Cloud Seed is available to a select group of users. If you are interested in joining this group, please fill in the [Trusted Testers invitation form](https://docs.google.com/forms/d/e/1FAIpQLSeJPtFE8Vpqs_YTAKkFK42p5mO9zIYA2jr_PiP2h32cs8R39Q/viewform) and we will reach out to you.

### Purpose

Deploying web application (and related workloads) from GitLab to major cloud providers should be trivial.

Cloud Seed makes it ridiculously simple and intuitive to consume appropriate Google Cloud services within GitLab.

### Why Google Cloud

_..or why not AWS or Azure?_

Cloud Seed is an open-source program that can be extended by anyone and we'd love to work with every major cloud provider.

The team at Google Cloud proved to be incredibly accessible, supportive and collaborative in this effort. Thus, Google Cloud.

As an open-source project, everyone can contribute and shape our direction.

### Everyone can contribute

There are several ways one may contribute to Cloud Seed. These are listed below:

* Become a Cloud Seed [Trusted Tester](https://docs.google.com/forms/d/e/1FAIpQLSeJPtFE8Vpqs_YTAKkFK42p5mO9zIYA2jr_PiP2h32cs8R39Q/viewform) in GitLab and [share feedback](https://gitlab.com/gitlab-org/incubation-engineering/five-minute-production/feedback/-/issues/new?template=general_feedback)
* If you are familiar with Ruby on Rails and/or Vue.js, consider [contributing to GitLab](https://docs.gitlab.com/ee/development/contributing/) as a developer. Much of Cloud Seed is an internal module within the GitLab code base
* If your familiarity lies with GitLab pipelines, consider contributing to the [Cloud Seed Library](https://gitlab.com/gitlab-org/incubation-engineering/five-minute-production/library) project
* If your expertize is in [Google Cloud APIs](https://cloud.google.com/apis) and Google Cloud technologies, reach out to us, we need you!

### Links

* Sign up as a [Trusted Tester](https://docs.google.com/forms/d/e/1FAIpQLSeJPtFE8Vpqs_YTAKkFK42p5mO9zIYA2jr_PiP2h32cs8R39Q/viewform)
* Read the [documentation](https://docs.gitlab.com/ee/cloud_seed/index.html)
* View the planning / issue [board](https://gitlab.com/groups/gitlab-org/incubation-engineering/five-minute-production/-/boards?group_by=epic)
* Or [submit feedback](https://gitlab.com/gitlab-org/incubation-engineering/five-minute-production/feedback/-/issues/new?template=general_feedback)

### Video Updates

A short, three minute end-to-end walkthrough for Cloud Seed:

<figure class="video_container">
    <iframe width="600" height="340" src="https://www.youtube.com/embed?max-results=1&controls=0&showinfo=0&rel=0&listType=playlist&list=PL05JrBw4t0Krf0LZbfg80yo08DW1c3C36" frameborder="0" allowfullscreen></iframe>
</figure>

View the [complete playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Krf0LZbfg80yo08DW1c3C36)
