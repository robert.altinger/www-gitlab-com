---
layout: handbook-page-toc
title: Home Page for Support's Kapok Group
description: Home Page for Support's Kapok Group
---

<!-- Search for all occurrences of NAME and replace them with the group's name.
     Search for all occurrences of URL HERE and replace them with the appropriate url -->

# Welcome to the home page of the Kapok group

Introductory text, logos, or whatever the group wants here

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Kapok resources

- Our Slack Channel: [spt_gg_kapok](https://gitlab.slack.com/archives/C03C6JQV3EZ)
- Our Team: [Kapok Members](https://gitlab-com.gitlab.io/support/team/sgg.html?search=kapok)

## Kapok workflows and processes

